package com.test.pageObject;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class JsonParser {
    static FileReader reader;

    public static String getValueByField(String key) throws IOException, ParseException {
        reader = new FileReader("/Users/user/Documents/com.test.pecodetest/src/main/resources/credentials");
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = (JSONObject) parser.parse(reader);
        return (String) jsonObject.get(key);
    }
}
