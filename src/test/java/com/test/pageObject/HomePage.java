package com.test.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@name=\"username\"]")
    private WebElement nameField;

    public WebElement getNameField() {
        return nameField;
    }

    @FindBy(xpath = "//*[@name=\"password\"]")
    private WebElement passwordField;

    public WebElement getPasswordField() {
        return passwordField;
    }

    @FindBy(xpath = "//*[@type=\"submit\"]")
    private WebElement signButton;

    public WebElement getSignButton() {
        return signButton;
    }


    public void openHomePage(WebDriver driver, String url) {
        driver.get(url);
    }

    public void closeHomePage(WebDriver driver) {
        driver.close();
    }

}
