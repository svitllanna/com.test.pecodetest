package com.test.seleniumTest;

import com.test.pageObject.HomePage;
import com.test.pageObject.JsonParser;
import com.test.pageObject.Utility;
import com.test.pageObject.WebDriverFactory;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertTrue;


public class SeleniumTests {

    WebDriver driver = WebDriverFactory.getDriver("chrome");
    HomePage homePage = new HomePage(driver);


    @Test(timeOut = 60000, priority = 1)
    public void seleniumFirstTest() throws IOException, ParseException {
        homePage.openHomePage(driver, "https://www.pecodesoftware.com/qa-portal/greet.php");
        WebElement explicitWait = (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"all\"]/div/center/h1")));
        WebElement element;
        element = homePage.getNameField();
        element.getText().contains("Username");
        element = homePage.getPasswordField();
        element.getText().contains("Password");
        homePage.getNameField().sendKeys(JsonParser.getValueByField("user"));
        homePage.getPasswordField().sendKeys(JsonParser.getValueByField("password"));
        homePage.getSignButton().click();
        assertTrue(homePage.getSignButton().isDisplayed(), "Login");
        element = driver.findElement(By.xpath("//*[@id=\"all\"]/div/center/h1"));
        element.getText().contains("AQA internship Login");
        element = driver.findElement(By.xpath("//img[@id='logomini']"));
        Boolean imageIsVisible = (Boolean) ((JavascriptExecutor) driver).executeScript("return arguments[0].complete " + "&& typeof arguments[0].naturalWidth != \"undefined\" " + "&& arguments[0].naturalWidth > 0", element);
        assertTrue(imageIsVisible.equals(true));
        element = driver.findElement(By.xpath("//span[@class=\"help-block\"]"));
        element.getText().contains("No account found with that username.");
        homePage.getNameField().clear();
        homePage.getNameField().sendKeys("test");
        homePage.getPasswordField().sendKeys(JsonParser.getValueByField("password"));
        homePage.getSignButton().click();
        element = driver.findElement((By.cssSelector("div.form-group.has-error")));
        element.getText().contains("The password you entered was not valid.");
    }

    @Test(timeOut = 60000, priority = 2)

    public void seleniumSecondTest() throws IOException, ParseException {
        homePage.getNameField().sendKeys(JsonParser.getValueByField("user"));
        homePage.getPasswordField().sendKeys(JsonParser.getValueByField("password"));
        homePage.getSignButton().click();
        WebElement element = homePage.getNameField();
        Boolean loginIsDisplayed = element.isDisplayed();
        assertTrue(loginIsDisplayed.equals(false));
    }

    @AfterMethod

    public void tearDown(ITestResult result) {
        if (ITestResult.FAILURE == result.getStatus()) {
            try {
                Utility.captureScreenshot(driver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @AfterTest

    public void endSession() {
        homePage.closeHomePage(driver);
    }
}
